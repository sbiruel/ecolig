;(function(){
  function id(v){return document.getElementById(v); }
  function loadbar() {
    var ovrl = id("overlayl"),
        prog = id("progress"),
        stat = id("progstat"),
        img = document.images,
        c = 0;
        tot = img.length;

    function imgLoaded(){
      c += 1;
      var perc = ((100/tot*c) << 0) +"%";
      prog.style.width = perc;
      stat.innerHTML = "Carregando "+ perc;
      if(c===tot) return doneLoading();
    }
    function doneLoading(){
      ovrl.style.opacity = 0;
      setTimeout(function(){ 
        ovrl.style.display = "none";
      }, 1200);
    }
    for(var i=0; i<tot; i++) {
      var tImg     = new Image();
      tImg.onload  = imgLoaded;
      tImg.onerror = imgLoaded;
      tImg.src     = img[i].src;
    }    
  }
  document.addEventListener('DOMContentLoaded', loadbar, false);
}()); 

$(function () {
    "use strict";

    $("#myVideo").on("loadstart", function () {
        console.log(3);
    });
});


//VIDEO
var video = document.getElementById("myVideo");
var btn = document.getElementById("myBtn");

function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}

jQuery.noConflict();



// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top -110
        }, 500, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

//SCROLL DOWN HIDE MENU

var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("header").style.top = "0";
  } else {
    document.getElementById("header").style.top = "-110px";
  }
  prevScrollpos = currentScrollPos;
};




//CHANGE BG

(function($) {
    
    $('#ligsperse').hover(function() {
        $('#brand-bg').css('background-image', 'url("images/aplication/bg-ligsperse.jpg")'),
        $('.img-brand').css('background', 'url("images/aplication/ligsperse-img.png")');
        $('#brand-info').css('background-color', '#f1e8fc');
        $('.text-brand').html('<p><strong class="ligsperse">Ligsperse</strong> apresenta excelente performance como dispersante para o mercado de construção civil. </p><p>Sendo um dos principais componentes da formulação de aditivos, promove a redução do consumo de água e de cimento através de seu efeito plastificante em misturas de concreto.</p>');
    });

    $('#ligseal').hover(function() {
        $('#brand-bg').css('background-image', 'url("images/aplication/bg-ligseal.jpg")'),
        $('.img-brand').css('background', 'url("images/aplication/ligseal-img.png")');
       $('#brand-info').css('background-color', '#fffdee');
        $('.text-brand').html('<p><strong class="ligseal">Ligseal</strong> pode ser empregado como alternativa sustentável à matérias-primas de origem fóssil na síntese de resinas fenólicas, entregando performance e tornando o produto dos nossos clientes mais amigáveis ao meio ambiente.</p>');
    });

    $('#ligflow').hover(function() {
        $('#brand-bg').css('background-image', 'url("images/aplication/bg-ligflow.jpg")'),
        $('.img-brand').css('background', 'url("images/aplication/ligflow-img.png")');
        $('#brand-info').css('background-color', '#e0fce4');
        $('.text-brand').html('<p><strong class="ligflow">Ligflow</strong> agrega benefícios como fluidez aos termoplásticos. Com esse atributo, essa solução pode ser aplicada e incorporada aos compostos para produzir peças plásticas.</p>');
    });

    $('#ligform').hover(function() {
        $('#brand-bg').css('background-image', 'url("images/aplication/bg-ligform.jpg")'),
        $('.img-brand').css('background', 'url("images/aplication/ligform-img.png")');
        $('#brand-info').css('background-color', '#cddbbd');
        $('.text-brand').html('<p><strong class="ligform">Ligform</strong> agrega benefício como rigidez aos termoplásticos. Com esse atributo, essa solução pode ser aplicada e incorporada aos compostos para produzir peças plásticas.</p>');
    });

    $('#ligflex').hover(function() {
        $('#brand-bg').css('background-image', 'url("images/aplication/bg-ligflex.jpg")'),
        $('.img-brand').css('background', 'url("images/aplication/ligflex-img.png")');
        $('#brand-info').css('background-color', '#f2f2f2');
        $('.text-brand').html('<p><strong class="ligflex">Ligflex</strong> é antioxidante para compostos de borracha, sendo fundamental para conferir durabilidade aos produtos desta indústria, como calçados, pneus, artefatos automotivos de borrachas e bandas de rodagem.</p>');
    });

})(jQuery);

//REMOVE CLASS
    $(".hexagon").hover(function(){
    $("a").removeClass("blink");
});

if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
    $('body').on("mousewheel", function () {
        // remove default behavior
        event.preventDefault(); 

        //scroll without smoothing
        var wheelDelta = event.wheelDelta;
        var currentScrollPosition = window.pageYOffset;
        window.scrollTo(0, currentScrollPosition - wheelDelta);
    });
}
